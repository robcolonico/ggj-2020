﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class music_controller : MonoBehaviour
{
    public FMOD.Studio.EventInstance musicToPlay;
    public int musicIndex = 0;
    public bool testBool = false;

    // Start is called before the first frame update
    void Start()
    {
        //Start the music, idiot
        musicToPlay = FMODUnity.RuntimeManager.CreateInstance("event:/music");
        musicToPlay.setParameterByName("act", musicIndex);
        musicToPlay.start();
    }

    // Update is called once per frame
    void Update()
    {
        if (testBool == true)
        {
            testBool = false;
            IncrementMusic();
        }
    }

    public void IncrementMusic()
    {
        musicIndex++;
        musicToPlay.setParameterByName("act", musicIndex);
    }

    public void setMusicIndex(int index)
    {
        //if (index > musicIndex)
        //{
        //    musicToPlay.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        //    while (musicIndex < index)
        //    musicIndex++;
        //    musicToPlay.setParameterByName("act", musicIndex);
        //}

        musicIndex = index;
        print("We set custom music");
        print(musicIndex);
        musicToPlay.setParameterByName("act", musicIndex);
    }
}
