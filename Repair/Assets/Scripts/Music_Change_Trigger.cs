﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Change_Trigger : MonoBehaviour
{

    public GameObject theCamera;
    private music_controller theMusicController;
    bool hasBeenTriggered = false;
    public int musicIndexToSet = -1;

    // Start is called before the first frame update
    void Start()
    {
        theMusicController = theCamera.GetComponent<music_controller>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasBeenTriggered == false)
        {
            if (collision.gameObject.tag == "Player")
            {
                hasBeenTriggered = true;
                if (musicIndexToSet != -1)
                {
                    print("Change the music to the custom index jerk");
                    theMusicController.setMusicIndex(musicIndexToSet);
                }
                else
                {
                    theMusicController.IncrementMusic();
                }

            }
        }

    }
}
