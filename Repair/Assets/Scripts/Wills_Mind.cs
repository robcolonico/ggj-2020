﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wills_Mind : MonoBehaviour
{
    //public bool metWill = true;
    public float roomWillTimer = 10.0f;
    public enum stateOfWill { asleep, active, angry, close };
    public stateOfWill currentState = stateOfWill.asleep;

    private float angryTime = 8.0f;
    private float closeTime = 5.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (currentState != stateOfWill.asleep && roomWillTimer > 0.0f)
        {
            roomWillTimer -= Time.deltaTime;
            WillMoodUpdate();
        }
    }

    public void WillMoodUpdate()
    {
        if (roomWillTimer <= 0.0f)
        {
            print("Ded");

        }
        else if (roomWillTimer <= closeTime)
        {
            currentState = stateOfWill.close;
        }
        else if (roomWillTimer <= angryTime)
        {
            currentState = stateOfWill.angry;
        }
        else
        {
            currentState = stateOfWill.active;
        }
    }

    /*
     * TODO
     * 
     * Final Object setup - Miller
     * Monster reveal logic - Miller 
     * VOTriggers are a bit early, move them more left for contextual stuff, do this only after we have a final resolution/build
     * reArrange Music Triggers - Miller
     * 
     * Drill audio - Mike/Coder
     * VO Remove ambient for the 2nd phase - Mike
     * Final machine being "fixed" final sound - Mike
     * Final machine being fixed partial sound - Mike
     * Monster VO Lines Effect more subtle, deeper if possible - Mike
     * VO Timing/Cleanup - Mike
     * Monster grab effect/sound - Mike
     * Any additiona monster chase sounds you feel are necessary - Mike
     * VO for Truth - Mike
     * 
     * Will Run Animation - Nate
     * Monster Grab - Nate
     * Monster Reveal animation - Nate
     * 
     * Zoom in issue because division for orthogonal is applied to both zoom in and out - Spencer
     * Lighting? - Spencer
     * 
     * Cleaning up the player character movement/feel - Rob
     * Start menu with start button - Rob
     * Game over screen - Rob
     * 
     * Final room - Coley
     * Cleaning up game scene to be playble start to finish - Coley
     * Background not being crap blue - Coley
     * Animation for Monster Run = Coley
     * 
     * Death room - Nate/???
     * 
     * 
     * DONE:
     * Going in and out of a vent OR cutting them - Spencer
     * Make default camera be the right one - Spencer
     * Environmental Lab Props - Nate
     * Shuttle you come in on - Nate
     * Game over screen - Miller
     * 3 Drill Final ROom Object - Miller
     * Create "Backward" VO Line trigger - Miller
     * Monster running down hallway - Miller
     * Place Music Triggers - Miller
     * Place VO lines - Miller
     * Idle Animation for Monster - Coley
     * Hallway - 29 squares long - Coley
     * Platform drilled - Miller
     * Ambient - Miller/Mike   
     * Music - Miller/Mike
     * Walk Cycle for character - Coley/Nate 
     * Player interacting with STUFF - Spencer
     * Door opening with drill
     * Playing VO *at all* - MILLER
     * Full layout for L1 - ???
     * VentCover - Rob
     * Vent can be opened (shut?) - Rob
     * 
     * 
     * 
     * CUT:
     * Ramps - Coley 
     * Strands that slow u - Coley/Nate?
     * Closing doors with drill - Spencer
     * Black out rooms you arent in - MILLER
     * Monster WANTS you to shut the doors or ELSE - Miller
     * Door can at least automatically close behind you - ???
     * 
     */


    /* Script
     * 
     * Act 1:
     * 
     * Truth: Sam, restore power to the station.
     * 
     * Will: *busy college hallway noises* Hi Sam, What do you want to do when you graduate?
     * 
     * Will: [AMBIENCE] What's your favorite flower Sam? I'm a bit of a green thumb (plant is quite dead)
     * 
     * Will: *bubbling/boiling water noises* Add some salt to taste. No Sam, it doesn't make it boil faster...
     * 
     * Will: Peekaboo Sam!!! *laughter* Got you!
     * 
     * Will: *BIRD NOISES*  Space Engineer Sam, mad scientist Will *chuckle* !!!
     * 
     * Truth: Sam, focus. *static* Drill *static* turn valve
     * 
     * 
     * Act 2:
     * 
     * /////Will: *laughter* I told you I was a mad scientist.
     * 
     * William: Doing the experiment in space is the only way to prove it.
     * 
     * William: I can change the world, I can fix everything.
     * 
     * William: Don't you want to go with me?
     * 
     * William: I launch tomorrow. Wish me luck Sam.
     * 
     * 
     * 
     * 
     * Truth: Sam, fix the panel
     * 
     * WILLIAM: You did it Sam, you saved me.
     * 
     * Truth: Sam, what's happening?
     * 
     * WILLIAM: Now let me save you.
     *
     * 
     * Act 3:
     * 
     * WILLIAM: Sam, Sam, Sam, Sam, Sam, Sam, Sam, Sam, Sam
     * 
     * WILLIAM: You know, I really missed you Sam.
     * 
     * WILLIAM: We're connected, Sam.
     * 
     * WILLIAM: Space Engineer Sam, and mAd sCiEnTISt Will
     * 
     * WILLIAM: Peekaboo Sam!!! *laughter* Got you!
     * 
     * WILLIAM: Hi Sam.
     * 
     * 
     * 
     */


}
