﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WillHallwayMind : MonoBehaviour
{
    public float currSpeed = -1;
    public float accelRate = -2.0f;
    public float clampSpeed = -50;

    public bool SamHugs = false;
    private bool hasBeenTriggered = false;
    public VO_Controller theVOController;

    public Canvas gameoverScreen;

    private SpriteRenderer spriteRenderer;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        theVOController = GameObject.Find("Main Camera").GetComponent<VO_Controller>();
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (SamHugs == true)
        {
           // print("Run bitch");
            //transform.Translate(currSpeed * Time.deltaTime, 0, 0);
            float randomFloat = Random.Range(-7, 7);
            currSpeed = Mathf.Clamp((currSpeed + accelRate), clampSpeed, 0);
            transform.Translate((currSpeed + randomFloat) * Time.deltaTime, 0, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasBeenTriggered == false)
        {
            if (collision.gameObject.tag == "Player")
            {
                theVOController.setVOIndexAndPlay(17);
                //GameObject.Find("Player").GetComponent<PlayerInput>().enabled = false;
                player.GetComponent<PlayerMovement>().enabled = false;
                player.GetComponent<PlayerInput>().enabled = false;
                player.GetComponent<Animator>().Play("Grabbed");

                gameoverScreen.enabled = true;
            }
        }
    }
}
