﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VO_Controller : MonoBehaviour
{
    public FMOD.Studio.EventInstance VOtoPlay;
    public int VOIndex = -1;
    public bool testBool = false;

    // Start is called before the first frame update
    void Start()
    {
        VOtoPlay = FMODUnity.RuntimeManager.CreateInstance("event:/vo");
        VOtoPlay.setParameterByName("voIndex", VOIndex);
       // VOtoPlay.start();
    }

    // Update is called once per frame
    void Update()
    {
        if (testBool == true)
        {
            testBool = false;
            IncrementVO();
            print("Test Bool Triggered");
        }
    }

    public void IncrementVO()
    {
        print("Increment voice called");
        //VOtoPlay.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        VOIndex++;
        VOtoPlay.setParameterByName("voIndex", VOIndex);
        VOtoPlay.start();
    }

    public void setVOIndexAndPlay(int index)
    {
        VOIndex = index;
        VOtoPlay.setParameterByName("voIndex", VOIndex);
        VOtoPlay.start();
    }
}
