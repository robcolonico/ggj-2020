using UnityEngine;
using System.Collections;

public class PlayerMovement : RaycastController
{
    bool inConversation;
    public bool inBuilding;
    bool mapOpen;
    public CollisionInfo collisions;
    public float jumpForgivnessTime;
    public float quickTurnForceScaler;
    public float climbSlopeExtraForceScaler;
    public float rollStrength;
    //input
    private Vector2 playerInput;
    public Vector2 outsideInputPushForce;
    Vector2 insideInputPushForce;
    public Vector2 moveForce;
    public float maxSpeed = 2f;
    Vector2 totalForce;
    float xInputDirection;
    float xDirection;
    Vector2 frictionCoefficent;
    public Vector2 groundFrictionCoefficent;
    public Vector2 airFrictionCoefficent;
    Vector2 friction;
    Vector2 currentVelocity;
    Vector2 acceleration;
    public float jumpHeight;
    public float timeToReachJumpApex;
    float jumpVelocity;
    float gravity;
    float moveDirforMoveToLoc;
    bool jumpPressed;
    bool rollPressed;
    bool rolling;
    public bool stunned;
    bool takeOff;
    float collisionTime;
    Vector3 moveloc;
    Animator anim;
    [HideInInspector]
    public float facedir;
    bool farFall;
    public bool flipped
    {
        get { return m_flipped; }
        set
        {
            if (m_flipped != value)
            {
                m_flipped = value;
                if (m_flipped == true)
                {
                    FlipCharacter();

                }
                else
                {
                    FlipCharacter();
                }
            }

            // trigger event (you could even compare the new value to
            // the old one and trigger it when the value really changed)
        }
    }
    bool m_flipped;
    Bounds bounds;
    SpriteRenderer spriteRenderer;
    // Use this for initialization
    public override void Start()
    {
        base.Start();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        anim = gameObject.GetComponent<Animator>();
        frictionCoefficent.x = groundFrictionCoefficent.x;
        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToReachJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToReachJumpApex;
        bounds = boxCollider.bounds;
    }
    void Animate()
    {
        float xdirectionlessSpeed = Mathf.Abs(currentVelocity.x);
        float ydirectionSpeed = currentVelocity.y;
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Attack") != true && anim.GetCurrentAnimatorStateInfo(0).IsName("SPINNER") != true && anim.GetCurrentAnimatorStateInfo(0).IsName("Spinnerotherway") != true)
        {
            if (!collisions.below)
            {

                collisionTime = Time.time + 0.6f;


                if (ydirectionSpeed > -0.1f)
                {
                    anim.Play("Jumping");
                    takeOff = true;

                }
                else if (ydirectionSpeed < -2)
                {
                    anim.Play("JumpDownwards");
                }
            }
            else
            {
                //if (takeOff)
                //{
                //    anim.Play("JumpLand");

                //    if (Time.time > collisionTime - 0.2f)
                //    {
                //        takeOff = false;
                //    }

                //}

                if (xdirectionlessSpeed > 1f && xdirectionlessSpeed < 4)
                {
                    anim.Play("Walk");
                }
                else if (xdirectionlessSpeed >= 3)
                {
                    anim.Play("Run");
                }
                else if (collisions.below)
                    anim.Play("Idle");


            }
        }

    }
    void Collision()
    {
        UpdateRaycastOrigins();
        CalculateRaySpacing();
        collisions.Reset();
        if (currentVelocity.x != 0)
        {
            SweeptestHorizontal(ref currentVelocity);
        }
        if (currentVelocity.y != 0)
        {
            SweeptestVertical(ref currentVelocity);
        }

    }
    void GetInput()
    {
        if (!rolling && !stunned)
        {
            playerInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
        else
        {
            playerInput = Vector2.zero;
        }
    
        if (!inBuilding)
        {
            if (playerInput.x >= -0.5f && playerInput.x <= 0.5f)
            {
                if (playerInput.x < 0)
                {
                    playerInput.x = -0.1f;
                }
                if (playerInput.x > 0)
                {
                    playerInput.x = 0.1f;
                }
            }
        }
        else
        {
            if (playerInput.x < 0)
            {
                playerInput.x = -1f;
            }
            if (playerInput.x > 0)
            {
                playerInput.x = 1f;
            }
        }
    }
    public void MoveToLocation(Vector3 moveLocation)
    {
        if (inConversation)
        {
            moveDirforMoveToLoc =  moveLocation.x - transform.position.x  >= 0 ? moveDirforMoveToLoc = 1 : moveDirforMoveToLoc = -1;
            moveloc = moveLocation;
            Debug.DrawRay(moveloc, Vector3.up*100, Color.red, 100);
        }
    }
    float timeAfterJump;
    void Move()
    {
        Animate();
        Collision();
        if(currentVelocity.y <= -25)
        {
            farFall = true;
        }
        if (collisions.below)
        {
            timeAfterJump = 0;
        }
        if (!collisions.below)
        {
            timeAfterJump += Time.deltaTime;
            if (timeAfterJump <= jumpForgivnessTime)
            {
                collisions.jump = true;
            }
            else
                collisions.jump = false;
        }
        if (collisions.above || collisions.below)
        {
            if (farFall)
            {
                //GetComponent<PlayerSounds>().PlayHardLanding();
                farFall = false;
            }
           //currentVelocity.y = 0;
        }
        // find direction of current movement
        Direction();
        //currentVelocity = currentVelocity * Time.deltaTime;
     
        GetComponent<Rigidbody2D>().MovePosition(GetComponent<Rigidbody2D>().position + currentVelocity * Time.deltaTime);
        Vector2 stopVel = currentVelocity;
        //getting inputs
        if (!inConversation && !mapOpen)
        {
            GetInput();
          }
        else if (inConversation)
        {
            if (collisions.below)
            {
                playerInput = new Vector3(moveDirforMoveToLoc, 0);
                float buffer = 0.2f;
                facedir = moveDirforMoveToLoc != 0 ? moveDirforMoveToLoc : facedir;
                if (Vector3.Distance(moveloc, (transform.position - Vector3.forward * transform.position.z)) < buffer)
                {
                    currentVelocity = Vector3.zero;
                    gameObject.GetComponent<SpriteRenderer>().flipX = (facedir == -1) ? true : false;
                    moveDirforMoveToLoc = 0;
                }
            }
            else
            {
                currentVelocity = Vector3.zero;
                gameObject.GetComponent<SpriteRenderer>().flipX = (facedir == -1) ? true : false;
                moveDirforMoveToLoc = 0;

            }
        }
        else
            playerInput = Vector2.zero;
                    
 
        // Total Force on the object
        friction = Vector2.Scale(-frictionCoefficent, currentVelocity);
        totalForce.y = moveForce.y + gravity;
        totalForce.x = moveForce.x;
        totalForce += friction;
        // the acceleration that the player receives
        acceleration = totalForce;

        //if input
        if (playerInput.x != 0 || stunned || rolling)
        {
            // set the friction coefficent much lighter so movement is much more fluid
            frictionCoefficent.x = (collisions.below) ? groundFrictionCoefficent.x : airFrictionCoefficent.x;
            // allowing for forward acceleration again
            if (xInputDirection != xDirection && Mathf.Abs(currentVelocity.x) > 1f)
            {
                moveForce.x = (outsideInputPushForce.x * quickTurnForceScaler) * playerInput.x ;
            }else if (collisions.climbingSlope)
            {
                moveForce.x = (outsideInputPushForce.x * climbSlopeExtraForceScaler) * playerInput.x;
            }
            else
            {
                moveForce.x = outsideInputPushForce.x * playerInput.x;
            }
        }
        else
        {
           // to stop the player in a specific distance no matter the speed
            if ((currentVelocity.x > 2f || currentVelocity.x < -2f))
            {
                float stopDist = 0.5f;
                frictionCoefficent.x = (collisions.below) ? Mathf.Abs(stopVel.x / (stopDist) * xDirection) : airFrictionCoefficent.x;

            }
            else
                frictionCoefficent.x = (collisions.below) ? groundFrictionCoefficent.x * 10f : airFrictionCoefficent.x;
            moveForce.x = 0;

        }
        
        currentVelocity += acceleration * Time.deltaTime;
        
        if (!inConversation && !mapOpen)
        {
            if (jumpPressed && (collisions.below||collisions.jump) && (currentVelocity.y <= 0.01f||collisions.climbingSlope))
            {
                anim.Play("JumpStart");
                Vector3 initPos = transform.position;
                currentVelocity.y = jumpVelocity;
                Debug.DrawRay(initPos, Vector3.up * jumpHeight, Color.green, 5);
                collisions.jump = false;
                timeAfterJump = jumpForgivnessTime + 1;
                jumpPressed = false;
            }
            if (rollPressed && (collisions.below || collisions.jump) && currentVelocity.y <= 0.01f)
            {
                EnterRoll();
            }
           
        }

    }
    void EnterRoll()
    {
      if (!rolling)
        {
     
            rolling = true;
            Bounds rollBounds = bounds;
            rollBounds.size = new Vector3(bounds.size.x, bounds.size.y / 2);
            Direction();
            // add a force quickly in the direction they are moving in
            currentVelocity.x = 0;            
            // resize the players collider to be smaller
            spriteRenderer.size = rollBounds.size;
            boxCollider.size = rollBounds.size;
            // make the player immune to damage for x time
            StartCoroutine(Roll());
            if (xDirection > 0)
            {
                anim.Play("SPINNER");
            }else
                anim.Play("Spinnerotherway");
            rollPressed = false;
            UpdateRaycastOrigins();
            CalculateRaySpacing();
        }
    }
    IEnumerator Roll()
    {
        yield return new WaitForEndOfFrame();
        AddForce(new Vector2(xDirection, 0), rollStrength);
        yield return new WaitForSeconds(0.3f);
        frictionCoefficent.x *= 2;
        ExitRoll();
    }
    void ExitRoll()
    {
        
        anim.Play("NORMAL");
        rolling = false;
        Bounds rollBounds = bounds;
        rollBounds.size = new Vector3(bounds.size.x, bounds.size.y);
        transform.position += new Vector3(0, bounds.size.y / 2, 0);
        frictionCoefficent.x = groundFrictionCoefficent.x;
        // resize the players collider to be regular
        spriteRenderer.size = rollBounds.size;
        boxCollider.size = rollBounds.size;
        UpdateRaycastOrigins();
        CalculateRaySpacing();
    }
    public void KnockBack(Vector2 forceDirection,float forceStrength, float stunDuration)
    {
        stunned = true;
        
        StartCoroutine(Stun(stunDuration, forceDirection.normalized, forceStrength));
    }
    IEnumerator Stun (float Duration,Vector2 forceDirection, float forceStrength)
    {
        yield return new WaitForEndOfFrame();
        currentVelocity += forceDirection * forceStrength;
        yield return new WaitForSeconds(Duration);
        stunned = false;
    }
    public void AddForce(Vector2 forceDirection, float forceStrength)
    {
        currentVelocity += forceDirection * forceStrength;
 
    }
    public void AddForce(Vector2 moveDir)
    {
        currentVelocity.y = 0;
        currentVelocity += moveDir/Time.deltaTime;
    }

    void FixedUpdate()
    {
        Move();
    }
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            jumpPressed = true;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            jumpPressed = false;
        }
        //if (Input.GetButtonDown("Fire2"))
        //{
        //    rollPressed = true;
        //}
        else if (Input.GetButtonUp("Fire2"))
        {
            rollPressed = false;
       
        }
    }
   public float Direction()
    {
     
            xInputDirection = Mathf.Sign(playerInput.x);
      
            foreach (SpriteRenderer renderer in gameObject.GetComponentsInChildren<SpriteRenderer>())
            {

                // renderer.flipX = (Mathf.Sign(currentVelocity.x) == 1) ? true : false; ;
             
                flipped = (Mathf.Sign(currentVelocity.x) == 1) ? false : true; ;


        }
            return xDirection = Mathf.Sign(currentVelocity.x);
        
    }
    void FlipCharacter()
    {
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }
    // This is a small easter egg

    public struct CollisionInfo
    {
        public bool jump;
        public bool climbingSlope;
        public bool above, below;
        public bool left, right;
        public float slopeAngle, slopeAngleOld;
        public void Reset()
        {
            above = below = false;
            left = right = false;
            jump = false;
            climbingSlope = false;
            slopeAngleOld = slopeAngle;
            slopeAngle = 0;
        }
    }

    void SweeptestVertical(ref Vector2 velocity) 
    {

        float directionY = Mathf.Sign(velocity.y);
        float rayLength = Mathf.Abs(velocity.y * Time.deltaTime) + skinWidth;
        for (int i = 0; i < vertRayCount; i++)
        {
            Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
            rayOrigin += Vector2.right * (vertRaySpacing * i + velocity.x * Time.deltaTime);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.up * directionY * rayLength, Color.cyan);
            if (hit)
            {

                if (hit.collider.isTrigger)
                {
                    return;
                }
                if (directionY == 1 && hit.collider.tag == "oneWay")
                {
                    return;
                }
                else
                {
                    collisions.above = directionY == 1;
                    collisions.below = directionY == -1;
                    velocity.y = ((hit.distance - skinWidth) * directionY);
                    rayLength = hit.distance;
                }

            }

        }
    }
    void ClimbSlope(ref Vector2 velocity, float slopeAngle)
    {
     
        float moveDistance = Mathf.Abs(velocity.x);
        float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
        if (velocity.y <= climbVelocityY)
            velocity.y = climbVelocityY;
        velocity.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(velocity.x);
        collisions.climbingSlope = true;
        collisions.below = true;
        collisions.slopeAngle = slopeAngle;
    }
    float maxClimbAngle = 80;
    void SweeptestHorizontal(ref Vector2 velocity)
    {

        float directionX = Mathf.Sign(velocity.x);
        float rayLength = Mathf.Abs(velocity.x * Time.deltaTime) + skinWidth;
        for (int i = 0; i < horizRayCount; i++)
        {
            Vector2 rayOrigin = (directionX == -1) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight;
            rayOrigin += Vector2.up * (horizRaySpacing * i);
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.right * directionX * rayLength, Color.red);
            if (hit)
            {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if(i==0 && slopeAngle <= maxClimbAngle)
                {
                    ClimbSlope(ref velocity, slopeAngle);
                }
                if (!collisions.climbingSlope || slopeAngle > maxClimbAngle)
                {
                    collisions.right = directionX == 1;
                    collisions.left = directionX == -1;
                    if (hit.collider.isTrigger)
                    {
                        return;
                    }
                    if (hit.collider.tag == "oneWay")
                    {

                        return;
                    }
                    else
                    {
                        velocity.x = ((hit.distance - skinWidth) * directionX);

                        rayLength = hit.distance;
                    }
                }
            }
        }
    }
   

}

