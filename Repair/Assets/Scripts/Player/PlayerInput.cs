﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PlayerMovement))]
public class PlayerInput : MonoBehaviour
{
    public Camera zoomedInCam;
    public Camera zoomedOutCam;
    public PlayerMovement movementScript;
    public GameObject drill;
    public Transform restPosition;
    public Transform activeRestPosition;
    private bool m_isTriggerheld;
    private bool m_isDrilling;
    private Vector2 currentArmfacingDirection;
    private Vector2 rightStickInputDirection;
    private float currentDrillArmAngle;
    private float rightStickInputAngle;
    float deadZonePercent = 0.2f;
    float drillarmMovedirection;
    public float armMoveSpeed;
    Animator armAnim;
    
    public FMOD.Studio.EventInstance drillEvent;
    public bool isDrilling
    {

        get { return m_isDrilling; }
        set
        {
            if (m_isDrilling != value)
            {
                m_isDrilling = value;
                if (m_isDrilling == true)
                {
                    drillEvent.setParameterByName("drillProgress", 0);
                    drillEvent.start();
                }
                if (m_isDrilling == false)
                {
                    drillEvent.setParameterByName("drillProgress", 1);
                }
            }
        }
    }
    public void ContactMade()
    {
        drillEvent.setParameterByName("contactMade", 1);
    }
    public void ExitContact()
    {
        drillEvent.setParameterByName("contactMade", 0);
    }
    public bool isTriggerHeld
    {
        get { return m_isTriggerheld; }
        set
        {
           if (m_isTriggerheld != value)
            {
                m_isTriggerheld = value;
                if (m_isTriggerheld == true)
                {
                    drill.GetComponent<Drill>().drillOut = true;
                    Camera.main.GetComponent<CameraSizeChange>().CameraResizeToReferenceCameraOnly(zoomedInCam);
                    Camera.main.GetComponent<CameraFollowScript>().zoomed = true;
                    movementScript.inBuilding = true;
                }
                else
                {
                    drill.GetComponent<Drill>().drillOut = false;
                    Camera.main.GetComponent<CameraSizeChange>().CameraResizeToReferenceCameraOnly(zoomedOutCam);
                    Camera.main.GetComponent<CameraFollowScript>().zoomed = false;
                    movementScript.inBuilding = false;
                }           
            }

            // trigger event (you could even compare the new value to
            // the old one and trigger it when the value really changed)
        }

    
    }
    // Start is called before the first frame update
    void Start()
    {
        drillEvent = FMODUnity.RuntimeManager.CreateInstance("event:/drill");
        armAnim = drill.GetComponent<Animator>();
        movementScript = gameObject.GetComponent<PlayerMovement>();
    }
    void DrillOut()
    {

    }
    void DrillIn()
    {

    }
    void GetInput()
    {
        if (Input.GetButton("Fire1") && Input.GetButton("Fire2"))
        {
            isDrilling = true;
            rightStickInputDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - drill.transform.position;
            armAnim.Play("ExtendedArm");
       
            if (movementScript.flipped == false && rightStickInputDirection.x <= 0)
            {
                rightStickInputDirection = new Vector2(0f, rightStickInputDirection.y <= 0 ? -1 : 1);
            }
            if (movementScript.flipped == true && rightStickInputDirection.x >= 0)
            {
                rightStickInputDirection = new Vector2(0f, rightStickInputDirection.y <= 0 ? -1 : 1);
            }
            //Debug.Log(rightStickInputDirection);

            Debug.DrawLine(drill.transform.position, rightStickInputDirection * 4);

            if (Vector3.SqrMagnitude(rightStickInputDirection) > deadZonePercent * deadZonePercent)
            {
                rightStickInputAngle = Mathf.Rad2Deg * Mathf.Atan2(rightStickInputDirection.y, rightStickInputDirection.x);

            }
            return;
        }
        if (isTriggerHeld == false)
        {
            Vector3 temp = restPosition.position - drill.transform.position;
            rightStickInputDirection = new Vector2(temp.x, temp.y);
            rightStickInputAngle = Mathf.Rad2Deg * Mathf.Atan2(rightStickInputDirection.y, rightStickInputDirection.x);
            return;
        }
        rightStickInputDirection = new Vector2(Input.GetAxisRaw("RightHorizontal"), Input.GetAxisRaw("RightVertical"));
        if (isTriggerHeld == true && Vector3.SqrMagnitude(rightStickInputDirection) < deadZonePercent * deadZonePercent)
        {
            armAnim.Play("RestingArm");
            isDrilling = false;
            Vector3 temp = activeRestPosition.position - drill.transform.position;
            rightStickInputDirection = new Vector2(temp.x, temp.y);
            rightStickInputAngle = Mathf.Rad2Deg * Mathf.Atan2(rightStickInputDirection.y, rightStickInputDirection.x);
            return;
        }
        isDrilling = true;
        armAnim.Play("ExtendedArm");
        
        if (movementScript.flipped ==false && Input.GetAxisRaw("RightHorizontal") <= 0)
        {
            rightStickInputDirection = new Vector2(0f, Input.GetAxisRaw("RightVertical") <= 0 ? -1 : 1);
        }
        if (movementScript.flipped == true && Input.GetAxisRaw("RightHorizontal") >= 0)
        {
            rightStickInputDirection = new Vector2(0f, Input.GetAxisRaw("RightVertical") <= 0 ? -1 : 1);
        }
        Debug.Log(rightStickInputDirection);

        Debug.DrawLine(drill.transform.position, rightStickInputDirection * 4);
      
        if (Vector3.SqrMagnitude(rightStickInputDirection) > deadZonePercent * deadZonePercent)
        {
            rightStickInputAngle = Mathf.Rad2Deg * Mathf.Atan2(rightStickInputDirection.y, rightStickInputDirection.x);         
            
        }

    }
        int GetTurnDirection(float angle)
        {

            if (angle < 0)
            {
         
            angle += 360;
            }

        if (angle < 10)
            {
                currentArmfacingDirection = rightStickInputDirection;
                return 0;

            }
            else if (angle > 180)
            {
                return -1;
            }
            else
                return 1;
        }
    
    // Update is called once per frame
    void Update()
    {
        currentArmfacingDirection = movementScript.flipped ? -drill.transform.right:drill.transform.right;
        currentDrillArmAngle = Mathf.Rad2Deg * Mathf.Atan2(currentArmfacingDirection.y, currentArmfacingDirection.x);
        GetInput();
        float inputToFacingDirAngle = rightStickInputAngle - currentDrillArmAngle;
        drillarmMovedirection = GetTurnDirection(inputToFacingDirAngle);

            drill.transform.Rotate(transform.forward, armMoveSpeed * drillarmMovedirection * Time.deltaTime);
        if (Input.GetAxisRaw("Fire2") >= 0.5f)
        {            
            isTriggerHeld = true;
            DrillOut();
        }
        else
        {
            isTriggerHeld = false;
            armAnim.Play("RestingArm");
            isDrilling = false;
            DrillIn();
        }
           
    }
}
