﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MovingPlatforms : RaycastController {
    
    public Vector2 speed;
    public LayerMask passengerMask;
    public bool theMovingScript;
    Rigidbody2D body;
    List<PassengerMovement> passengerMovement;
    public Transform spine;
    Vector2  currentVelocity;
    Vector3  previousPosition;
  
	// Use this for initialization
	public override void Start () {
        base.Start();
       body = gameObject.GetComponent<Rigidbody2D>();
       
        currentVelocity = Vector2.zero;
    }
	void OnEnable()
    {
     
    }
    void OnDisable()
    {
        if (passengerMovement != null)
        {
           
        }
            passengerMovement = null;
    }
    
	// Update is called once per frame
	void Update () {
        
       

    }
    private void FixedUpdate()
    {
        UpdateRaycastOrigins();
        CalculatePassengerMovement(currentVelocity);
        MovePassengers(true);
        currentVelocity = (previousPosition - transform.position );
        MovePassengers(false);
        previousPosition = transform.position;
    }
    void MovePassengers(bool beforeMovePlatform)
    {
        foreach(PassengerMovement passenger in passengerMovement)
        {
            if(passenger.moveBeforePlatform == beforeMovePlatform)
            {
              
                passenger.transform.position -= passenger.velocity;
             
            }
        }
    }
    void CalculatePassengerMovement(Vector3 velocity)
    {
       
        HashSet<Transform> movedPassengers = new HashSet<Transform>();
        passengerMovement = new List<PassengerMovement>();
        float directionX = Mathf.Sin(velocity.x);
        float directionY = Mathf.Sin(velocity.y);
        //vertically moving platform
        if (velocity.y != 0)
        {
            float rayLength = Mathf.Abs(velocity.y) + skinWidth;

            for (int i = 0; i < vertRayCount; i++)
            {
                Vector2 rayOrigin = (directionY == -1) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft;
                rayOrigin += Vector2.right * (vertRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);
                Debug.DrawRay(rayOrigin, Vector2.up * directionY * 10, Color.cyan);
                if (hit)
                {
                    if (!movedPassengers.Contains(hit.transform))
                    {
                        movedPassengers.Add(hit.transform);

                        float pushY = velocity.y + Mathf.Epsilon - (hit.distance - skinWidth) * directionY;
                        passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(0, pushY), true, true));

                    }
                }
            }
        }
    }
    struct PassengerMovement
    {
        public Transform transform;
        public Vector3 velocity;
        public bool standingOnPlatform;
        public bool moveBeforePlatform;
        public PassengerMovement(Transform _transform, Vector3 _velocity, bool _standingonPlatform, bool _moveBeforePlatform)
        {
            transform = _transform;
            velocity = _velocity;
            standingOnPlatform = _standingonPlatform;
            moveBeforePlatform = _moveBeforePlatform;
           
        }
    }
}
