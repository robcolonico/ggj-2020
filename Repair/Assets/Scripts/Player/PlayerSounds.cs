﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;   
public class PlayerSounds : MonoBehaviour {
    public List<AudioClip> FootstepAudioBank;
    public List<AudioClip> LandingAudioBank;
    AudioSource playerAudioSource;
	// Use this for initialization
	void Start () {
        playerAudioSource = GetComponent<AudioSource>();
	}
	public void PlayFootStep()
    {
        playerAudioSource.PlayOneShot(FootstepAudioBank[Random.Range(0, FootstepAudioBank.Count)]);
    }
    public void PlayHardLanding()
    {
        playerAudioSource.PlayOneShot(LandingAudioBank[Random.Range(0, LandingAudioBank.Count )]);
    }
	// Update is called once per frame
	void Update () {
	
	}
}
