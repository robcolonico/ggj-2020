﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrapping : MonoBehaviour {
    public GameObject dropOffPoint;
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "Player")
        {
            col.transform.position = dropOffPoint.transform.position; 
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
