﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VO_Change_Trigger : MonoBehaviour
{
    public GameObject theCamera;
    private VO_Controller theVOController;
    bool hasBeenTriggered = false;
    public int VOIndexToSet = -1;
    public bool waitForWill = false;
    private WillHallwayMind WillsMind;

    // Start is called before the first frame update
    void Start()
    {
        theVOController = theCamera.GetComponent<VO_Controller>();
        WillsMind = GameObject.Find("willIAM").GetComponent<WillHallwayMind>();
    }

    // Update is called once per frame
    void Update()
    {
        

           
    }

    void collisionCheck(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            hasBeenTriggered = true;
            if (VOIndexToSet != -1)
            {
                theVOController.setVOIndexAndPlay(VOIndexToSet);
            }
            else
            {
                print("Asked to play voice increment");
                theVOController.IncrementVO();
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasBeenTriggered == false)
        {
            if (waitForWill == true)
            {
                if (WillsMind.SamHugs == true)
                {
                    collisionCheck(collision);
                }

            }
            else
            {
                collisionCheck(collision);
            }
        }
    }
}
