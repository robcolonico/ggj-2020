﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drill : MonoBehaviour
{
    public bool drillOut;
    public int drillState;
    public PlayerInput inputscript;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Narrative")
        {
            inputscript.ContactMade();
            collision.gameObject.SendMessage("StartBeingDrilled", drillState);
        }

    }
    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag != "Narrative")
        {
            inputscript.ExitContact();
            collision.gameObject.SendMessage("StopBeingDrilled", drillState);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
