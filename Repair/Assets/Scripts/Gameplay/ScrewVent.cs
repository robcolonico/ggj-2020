﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrewVent : MonoBehaviour
{
    public GameObject    screw;
    public GameObject    vent;
    public float         timeToFullyOpenDoor;
    bool                 beingDrilled;
    [Range(0.0f, 1.0f)]
    public float         progress;
    public bool          reverse;

    private bool         opened = false;
    private float        screwYStart;
    public  float        screwTravel = 0.5f;
    private Vector3      screwPosition;
    private Rigidbody2D  screwRB;
    private Rigidbody2D  ventRB;

    void Start()
    {
        screwRB = screw.GetComponent<Rigidbody2D>();
        ventRB  = GetComponent<Rigidbody2D>();
        screw.transform.localRotation = transform.localRotation;
        screw.transform.Rotate(new Vector3(0, 0, reverse ? -90 : 90));
        screw.transform.parent = transform;
        screw.transform.localPosition = vent.transform.localPosition;

        screwPosition = screw.transform.localPosition;
        screwPosition.y += reverse ? 0.2f : -0.2f;
        screw.transform.localPosition = screwPosition;
        screwYStart   = screwPosition.y;

        screwRB.simulated = false;
        ventRB.isKinematic  = true;
    }
    void StartBeingDrilled()
    {
        timeToFullyOpenDoor = (1 - progress) * timeToFullyOpenDoor;
        StartCoroutine(OpenDoor(timeToFullyOpenDoor));
        beingDrilled = true;
    }
    void StopBeingDrilled()
    {
        StopAllCoroutines();
        beingDrilled = false;
    }
    IEnumerator OpenDoor(float timeToLerp)
    {
        float currentprogress = progress;
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime * rate;
            fasterTime += Time.deltaTime * rate;

            progress = Mathf.Lerp(currentprogress, 1, currentTime);

            yield return null;
        }
      
    }
    void Update()
    {
        if (!opened)
        {
            float travel = reverse ? screwYStart + screwTravel : screwYStart - screwTravel;
            screwPosition.y = Mathf.Lerp(screwYStart, travel, progress);
            screw.transform.localPosition = screwPosition;

            if (progress >= 1)
            {
                opened = true;
                screw.transform.parent = null;
                screwRB.simulated  = true;
                ventRB.isKinematic = false;
                vent.GetComponent<BoxCollider2D>().enabled = false;
                screwRB.AddTorque(0.3f);
                ventRB.AddTorque(-500f);
                Debug.Log("Vent opened.");
            }
        }
    }
}
