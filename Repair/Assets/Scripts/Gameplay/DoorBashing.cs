﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBashing : MonoBehaviour
{
    public FMOD.Studio.EventInstance bashSound;

    public Sprite firstDoorBash;
    public Sprite lastDoorBash;
    public bool firstDoorBashed = false;
    public bool lastDoorBashed = false;

    private float temp = 5.0f;
    public bool debugBash = false;

    // Start is called before the first frame update
    void Start()
    {
        bashSound = FMODUnity.RuntimeManager.CreateInstance("event:/doorBash");
       // firstBashSound.set3DAttributes();
    }

    // Update is called once per frame
    void Update()
    {

        if (debugBash == true)
        {
            debugBash = false;
            if (firstDoorBashed == false)
            {
                FirstBash();
            }
            else if (lastDoorBashed == false)
            {
                LastBash();
            }
            else
            {
                bashSound.start();
            }
        }
        //temp -= Time.deltaTime;
        //
        // if (temp <= 2.0f)
        //{
        //    LastBash();
        //}
        //else if ( temp <= 4.0f)
        //{
        //    FirstBash();
        //}
    }

    void SealDoor()
    {
        this.GetComponentInParent<ScrewDoor>().enabled = false;
        //Play Sound
    }

    public void FirstBash ()
    {
        if (firstDoorBashed == false)
        {
            firstDoorBashed = true;
            this.GetComponent<SpriteRenderer>().sprite = firstDoorBash;
            bashSound.start();
        }
    }

    public void LastBash()
    {
        if (lastDoorBashed == false)
        {
            lastDoorBashed = true;
            this.GetComponent<SpriteRenderer>().sprite = lastDoorBash;
            bashSound.start();
        }
    }
}
