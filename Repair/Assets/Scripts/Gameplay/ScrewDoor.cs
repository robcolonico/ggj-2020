﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrewDoor : MonoBehaviour
{
    public GameObject   leftValve;
    public GameObject   door;
    public float        timeToFullyOpenDoor;
    bool                beingDrilled;  
    [Range(0.0f, 1.0f)]
    public  float       progress = 0;
    private float       progressCache;

    private bool        opened = false;
    private float       doorYStart;
    public  float       doorTravel = 2;
    private Vector3     doorPosition;
    private Animator    leftValveAnim;

    // FMOD
    public FMOD.Studio.EventInstance doorEvent;

    void Start()
    {
        leftValveAnim = leftValve.GetComponent<Animator>();
        doorPosition  = door.transform.localPosition;
        doorYStart    = doorPosition.y;
        doorEvent     = FMODUnity.RuntimeManager.CreateInstance("event:/door");
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
     
     
    }
    void OnTriggerExit2D(Collider2D collision)
    {

    }
    void StartBeingDrilled(int drillState)
    {
        doorEvent.start();
        timeToFullyOpenDoor = (1 - progress) * timeToFullyOpenDoor;
        StartCoroutine(OpenDoor(timeToFullyOpenDoor));
        beingDrilled = true;
       
    }
    void StopBeingDrilled(int drillState)
    {
        doorEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        StopAllCoroutines();
        leftValveAnim.Play("Idle");
        beingDrilled = false;
    }
    IEnumerator OpenDoor(float timeToLerp)
    {
        leftValveAnim.Play("ValveAnimation");

        float currentprogress = progress;
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
            {
                currentTime += Time.deltaTime * rate;
                fasterTime += Time.deltaTime * rate;
                
                progress = Mathf.Lerp(currentprogress, 1, currentTime);

                yield return null;
            }
        leftValveAnim.Play("Idle");

    }
    void Update()
    {
        if (beingDrilled == true)
        {
            //BeingDrilled();
        }
        doorPosition.y = Mathf.Lerp(doorYStart,   doorYStart   + doorTravel,   progress);
        door.transform.localPosition  = doorPosition;

        if (progress >= 1.0 && !opened)
        {
            opened = true;
            doorEvent.setParameterByName("progress", 1);
            Debug.Log("Door is opened.");
        }
        else if (progress <= 0 && opened)
        {
            opened = false;
            doorEvent.setParameterByName("progress", 1);
            Debug.Log("Door is closed.");
        }
        else if (progress > 0 && progressCache <= 0)
        {
           
            Debug.Log("Door is opening.");
        }
        else if (progress < 1 && progressCache >= 1)
        {
            Debug.Log("Door is closing.");
        }

        progressCache = progress;
    }
}
