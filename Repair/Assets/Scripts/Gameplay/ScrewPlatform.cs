﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrewPlatform : MonoBehaviour
{
    [Range(0.0f, 1.0f)]
    public float progress = 0.5f;
    public float platformTravel = 2f;

    private float xStart;
    private Vector3 platformPosition;

    public float timeToFullyMovePlatform;

    void StartBeingDrilled(int drillState)
    {
        //print("We drillin' a platform");
        timeToFullyMovePlatform = (1 - progress) * timeToFullyMovePlatform;
        StartCoroutine(MovePlatform(timeToFullyMovePlatform));
        //beingDrilled = true;
    }
    void StopBeingDrilled(int drillState)
    {
        StopAllCoroutines();
        //leftValveAnim.Play("Idle");
       // beingDrilled = false;
    }
    IEnumerator MovePlatform(float timeToLerp)
    {
        //leftValveAnim.Play("ValveAnimation");

        float currentprogress = progress;
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime * rate;
            fasterTime += Time.deltaTime * rate;

            progress = Mathf.Lerp(currentprogress, 1, currentTime);

            yield return null;
        }
        //leftValveAnim.Play("Idle");

    }

    void Start()
    {
        platformPosition = transform.position;
        xStart = platformPosition.x - platformTravel;
    }

    void Update()
    {
        platformPosition.x = Mathf.Lerp(xStart, xStart + (platformTravel * 2), progress);
        transform.position = platformPosition;
    }



}
