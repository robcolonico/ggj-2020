﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Timer : MonoBehaviour
{
    public float presetRoomTime = 20.0f;
    public Wills_Mind masterMind;
    public bool hasBeenTriggered = false;

    // Start is called before the first frame update
    void Start()
    {
        masterMind = GameObject.Find("Wills_Mind").GetComponent<Wills_Mind>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasBeenTriggered == false)
        {
            if (masterMind.currentState != Wills_Mind.stateOfWill.asleep)
            {
                if (collision.gameObject.tag == "Player")
                {
                    hasBeenTriggered = true;
                    updateWilliamTimer();
                }
            }
        }
    }

    private void FixedUpdate()
    {

    }

    private void updateWilliamTimer()
    {
        print("Will has been updated");
        masterMind.roomWillTimer = presetRoomTime;
    }
}
