﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour {
    public Camera sizeCam;
    public bool release;
    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.tag == "Player" )
        {
            if (release)
            {
                Camera.main.GetComponent<CameraSizeChange>().ResizeButFollowX(sizeCam);
            }else
                Camera.main.GetComponent<CameraSizeChange>().CameraResizeToReferenceCamera(sizeCam);      
        }
    }
 
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
