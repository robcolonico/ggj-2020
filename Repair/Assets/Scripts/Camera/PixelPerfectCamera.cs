﻿using UnityEngine;
using System.Collections;

public class PixelPerfectCamera : MonoBehaviour
{
	private PlayerMovement playerScript; // reference to the Player's behaviour script	
    public bool followPlayer;
      // reference to Player game object needed to have camera follow player
		public GameObject focus;
    public void Awake()
    {
        // this is the import setting on Sprites
        float pixelsToUnits = 350f;
        // Set the orthographic size of the camera so that bitmap art is exactly to scale
        GetComponent<Camera>().orthographicSize = (Screen.height / pixelsToUnits / 2.0f);
    }
    void Start ()
		{
				// Get a reference to the Player Game object
				focus = GameObject.Find ("Player").transform.GetChild(0).gameObject;
				
		}

		void Update ()
		{
      

        }
		// move camera in LateUpdate to ensure that all game objects have been updated before
		void LateUpdate ()
		{

        float cameraY = focus.transform.position.y;
				// force camera to stay above a minimum vertical value
				// *place-holder* value set here
				if (cameraY <0f) {
			cameraY = 0f;
		}

		if (cameraY < -.5f) {
						cameraY = -.5f;
				}

        // Set the position of the camera to the to the position of the player
        if (followPlayer)
            transform.position = new Vector3(focus.transform.position.x, focus.transform.position.y, transform.position.z);
    }
}
