﻿using UnityEngine;
using System.Collections;

public class CameraCopy : MonoBehaviour {
    Camera cameraToCopy;
    Camera thisCam;
	// Use this for initialization
	void Start () {
        cameraToCopy = Camera.main;
        thisCam = gameObject.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        thisCam.orthographicSize = cameraToCopy.orthographicSize;
	}
}
