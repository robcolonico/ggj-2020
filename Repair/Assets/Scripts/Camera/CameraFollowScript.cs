﻿using UnityEngine;
using System.Collections;

public class CameraFollowScript : MonoBehaviour {
    public GameObject focus;
    public bool followFocus;
    public bool followY=true;
    public bool followX=true;
    public bool zoomed;
    public Vector3 desiredCamPosition;
    float yOffset;
    float XOffset;
    public void Awake()
    {
        // this is the import setting on Sprites
        float pixelsToUnits = 25f;
        // Set the orthographic size of the camera so that bitmap art is exactly to scale
        GetComponent<Camera>().orthographicSize = (12);
        
    }


    // Update is called once per frame
    void Update () {
        if (!zoomed)
        {
            yOffset = (GetComponent<Camera>().orthographicSize / 4f);
        }
        else
        {
            yOffset = (GetComponent<Camera>().orthographicSize / 4f);
        }
        XOffset = (GetComponent<Camera>().orthographicSize / 0.85f);
             Vector3 desiredCamPosition = new Vector3(focus.transform.position.x + XOffset, focus.transform.position.y + yOffset, -10);
        // Set the position of the camera to the to the position of the focus
        if (followFocus)
            if (followX && followY)
            {
                transform.position = new Vector3(focus.transform.position.x+XOffset, focus.transform.position.y+yOffset, -10);
            }else if (followY)
            {
                transform.position = new Vector3(transform.position.x, focus.transform.position.y+3f , -10);
            }else if (followX)
            {
                transform.position = new Vector3(focus.transform.position.x, transform.position.y, -10);
            }
    }
}

