﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraSizeChange : MonoBehaviour
{
    public Vector3 cameraLoc;
    public Vector3 camLocTrue;
    public List<Transform> transformsToFrame;
    public CameraFollowScript followScript;
    public Transform player;
    float _cameraSize;
    public float cameraSize;
    public float lerpTime;
    Camera currentCameraForSizing;
    // Use this for initialization
    void Start()
    {
        followScript = gameObject.GetComponent<CameraFollowScript>();
        cameraSize = Camera.main.orthographicSize;
        float _cameraSize;
    }
    //If one thing is added through script
    public void CameraFrameObjects(Transform thingToFrame)
    {
        followScript.followFocus = false;
        List<Transform> frame;
        frame = new List<Transform>();
        frame.AddRange(transformsToFrame);
        frame.Add(thingToFrame);
        Bounds cameraRectBounds = new Bounds(transformsToFrame[0].position, Vector3.zero);
        for (int i = 0; i < frame.Count; i++)
        {
            cameraRectBounds.Encapsulate(frame[i].GetComponent<Renderer>().bounds);
        }
        Vector3 midpoint = cameraRectBounds.center - Vector3.forward * 10;
        StartCoroutine(ChangeCameraLoc(transform.position, midpoint, Camera.main.orthographicSize, cameraRectBounds.extents.x * 2, lerpTime));
    }
    //if multiple things are added through scripts
    public void CameraFrameObjects(List<Transform> thingsToFrame)
    {
        followScript.followFocus = false;
        thingsToFrame.AddRange(transformsToFrame);
        Bounds cameraRectBounds = new Bounds(transformsToFrame[0].position, Vector3.zero);
        for (int i = 0; i < thingsToFrame.Count; i++)
        {
            if (thingsToFrame[i].GetComponent<Renderer>())
            {
                cameraRectBounds.Encapsulate(thingsToFrame[i].GetComponent<Renderer>().bounds);
            }
            else if (thingsToFrame[i].GetComponent<CanvasRenderer>())
            {
               cameraRectBounds.Encapsulate(thingsToFrame[i].transform.position);
            }
        }
        Vector3 midpoint = cameraRectBounds.center - Vector3.forward * 10;
        StartCoroutine(ChangeCameraLoc(transform.position, midpoint, Camera.main.orthographicSize, cameraRectBounds.extents.x/2 +cameraRectBounds.extents.y, lerpTime));
    }
    public void ReleaseCameraFraming()
    {
        if (followScript.followFocus == true)
        {
            return;
        }
        currentCameraForSizing = Camera.main;
        if (_cameraSize <= 0)
        {
            _cameraSize = 4.5f;
        }
        StartCoroutine(ChangeCameraLoc(transform.position, followScript.focus.transform.position - Vector3.forward * 10, Camera.main.orthographicSize, _cameraSize, lerpTime));
        FollowFocus();
    }
    public void ReleaseCameraFramingForExitConversation()
    {
        followScript.followFocus = true;
        if (currentCameraForSizing != Camera.main && currentCameraForSizing!=null)
        {
            StartCoroutine(ChangeCameraLoc(new Vector3(transform.position.x, currentCameraForSizing.transform.position.y, -10), new Vector3(transform.position.x, currentCameraForSizing.transform.position.y, -10), Camera.main.orthographicSize, currentCameraForSizing.orthographicSize, lerpTime));
        }
        else if (currentCameraForSizing!=null)
        {
            followScript.followX = true;
            StartCoroutine(ChangeCameraLoc(new Vector3(transform.position.x, currentCameraForSizing.transform.position.y, -10), new Vector3(transform.position.x, currentCameraForSizing.transform.position.y, -10), Camera.main.orthographicSize, _cameraSize, lerpTime));
        }else
        {
            _cameraSize = cameraSize;
            print(_cameraSize);
            ReleaseCameraFraming();
        }
        }
    public void CameraResizeToReferenceCamera(Camera sizingCamera)
    {
       followScript.followFocus = false;
       StartCoroutine(ChangeCameraLoc(new Vector3(transform.position.x, transform.position.y, -10), new Vector3(sizingCamera.transform.position.x, sizingCamera.transform.position.y,-10),Camera.main.orthographicSize,sizingCamera.orthographicSize,lerpTime));
    }
    public void CameraResizeToReferenceCameraOnly(Camera sizingCamera)
    {
        followScript.followFocus = true;
        StopAllCoroutines();
        StartCoroutine(ChangeCameraSize(new Vector3(transform.position.x, transform.position.y, transform.position.z), true, Camera.main.orthographicSize, sizingCamera.orthographicSize, lerpTime));
    }
    public void ResizeButFollowX(Camera sizingCamera)
    {
       
        currentCameraForSizing = sizingCamera;
        followScript.followY = false;
        followScript.followFocus = true;
        StartCoroutine(ChangeCameraLoc(new Vector3 (transform.position.x, sizingCamera.transform.position.y,transform.position.z), true, Camera.main.orthographicSize, sizingCamera.orthographicSize, lerpTime));
        

    }
    public void ResizeButFollowXwithOffset(Camera sizingCamera, float offset)
    {

        currentCameraForSizing = sizingCamera;
        followScript.followY = false;
        followScript.followFocus = true;
        StartCoroutine(ChangeCameraLoc(new Vector3(transform.position.x, sizingCamera.transform.position.y, transform.position.z), true, Camera.main.orthographicSize, sizingCamera.orthographicSize, lerpTime));

    }
    public void ResizeButFollowY(List<Transform> thingsToFrame)
    {
        followScript.followX = false;
        CameraFrameObjects(thingsToFrame);
        followScript.followFocus = true;
    }
    public void FollowFocus()
    {
        followScript.followFocus = true;
        followScript.followX = true;
        followScript.followY = true;
    }
    IEnumerator ChangeCameraLoc(Vector3 startPos, Vector3 endPos, float startSize, float endSize, float timeToLerp)
    {
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime * rate;
            fasterTime += Time.deltaTime * rate;
            Camera.main.transform.position = Vector3.Lerp(startPos, endPos, currentTime);
            Camera.main.orthographicSize = Mathf.Lerp(startSize, endSize, currentTime);

            yield return null;
        }
        cameraLoc = startPos;
        //size it goes back to
        _cameraSize = cameraSize ;

    }
    IEnumerator ChangeCameraLoc(Vector3 startPos, bool followFocus, float startSize, float endSize, float timeToLerp)
    {
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime * rate;
            fasterTime += Time.deltaTime * rate;
            Camera.main.transform.position = Vector3.Lerp(startPos, new Vector3 (followScript.focus.transform.position.x, startPos.y, startPos.z), currentTime);
            Camera.main.orthographicSize = Mathf.Lerp(startSize, endSize, currentTime);

            yield return null;
        }
        cameraLoc = startPos;
        //size it goes back to
        _cameraSize = cameraSize;

    }
    IEnumerator ChangeCameraSize(Vector3 startPos, bool followFocus, float startSize, float endSize, float timeToLerp)
    {
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime * rate;
            fasterTime += Time.deltaTime * rate;
            //Camera.main.transform.position = Vector3.Lerp(startPos, new Vector3(startPos.x, followScript.desiredCamPosition.y, startPos.z), fasterRate);
            Camera.main.orthographicSize = Mathf.Lerp(startSize, endSize, currentTime);

            yield return null;
        }
        cameraLoc = startPos;
        //size it goes back to
        _cameraSize = cameraSize;

    }

}
