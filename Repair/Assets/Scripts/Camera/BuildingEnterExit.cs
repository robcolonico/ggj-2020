﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
[RequireComponent(typeof(BoxCollider2D))]
public class BuildingEnterExit : MonoBehaviour {

    public Transform exit;
    public Image indicator;
    public Image enterindicator;
    public float entranceRate;
    public Image screenFadePanel;
    public bool entering;
    public bool startCameraLock;
   public  float enter = 0;
    public float goToTP;
    float fadetime = 1;
    BoxCollider2D boxCollider;
    public GameObject player;
    bool teleporting;
    public Camera sizeCam;
	// Use this for initialization
	void Start () {
        if (startCameraLock)
        {
            Camera.main.GetComponent<CameraSizeChange>().ResizeButFollowX(sizeCam);
        }
        boxCollider = gameObject.GetComponent<BoxCollider2D>();
        //screenFadePanel.gameObject.SetActive(false);
        if (!boxCollider.isTrigger)
            boxCollider.isTrigger = true;
      
	}
    void Teleport()
    {   
        if (Time.timeScale != 0)
        {
            goToTP += Time.deltaTime / Time.timeScale;
        }
        if(goToTP >= fadetime+0.3f)
        {
            if (entering)
            {
                Camera.main.GetComponent<CameraSizeChange>().ResizeButFollowX(sizeCam);
                player.transform.position = new Vector3(exit.position.x, exit.position.y, 0);
             
            }
            else
            {
                Camera.main.GetComponent<CameraSizeChange>().ReleaseCameraFraming();
                player.transform.position = new Vector3(exit.position.x,exit.position.y,0);

            }
        }
        if (goToTP >= fadetime+1)
        {
            enter = 0;
            goToTP = 0;
            indicator.gameObject.SetActive(false);
            enterindicator.fillAmount = 0;
            enterindicator.gameObject.SetActive(false);
            screenFadePanel.CrossFadeAlpha(1, fadetime, true);
            teleporting = false;
            player = null;
        }
     }
	void Left()
    {
        if (!teleporting)
        {
            player = null;
        }
    }
	// Update is called once per frame
	void Update () {
        enter = Mathf.Clamp(enter, 0, 2); 
        
            Teleport();

        if (player != null)
        {
            indicator.gameObject.SetActive(true);
            enterindicator.gameObject.SetActive(true);
           //if (Input.GetAxis("Vertical") > 0.1f && player.GetComponent<PlayerInteraction>().currentPlayerState != PlayerInteraction.playerState.conversing)
           // {
           //     enter += entranceRate * Time.deltaTime;
           //     enterindicator.fillAmount = enter;
           //     if (enter >= 1)
           //     {
           //         screenFadePanel.gameObject.SetActive(true);
           //         // alpha must start at 1 on target image or it wont work
           //         screenFadePanel.CrossFadeAlpha(255, fadetime, true);
           //        teleporting = true;

           //     }

           // }else
           // enter -= entranceRate * Time.deltaTime;
           // enterindicator.fillAmount = enter;
        } else
        {
            enter -= entranceRate * Time.deltaTime;
            indicator.gameObject.SetActive(false);
            enterindicator.gameObject.SetActive(false);
        }

    }
  
}
