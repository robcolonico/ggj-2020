﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WillHugZone : MonoBehaviour
{

    public bool hasBeenTriggered = false;
    public float downTimer = 1.5f;
    public VO_Controller theVOController;
    public FMOD.Studio.EventInstance WillsVoice;

    // Start is called before the first frame update
    void Start()
    {
        WillsVoice = FMODUnity.RuntimeManager.CreateInstance("event:/monster");
       // WillsVoice.setParameterByName("voIndex", VOIndex);
    }

    // Update is called once per frame
    void Update()
    {
        if (hasBeenTriggered == true && downTimer > 0.0f)
        {
            downTimer -= Time.deltaTime;
            if (downTimer <= 0.0f)
            {
                GameObject.Find("willIAM").GetComponent<Animator>().Play("William_Run");
                print("HUGS");
                GameObject.Find("willIAM").GetComponent<WillHallwayMind>().SamHugs = true;

                //Change ambience
                WillsVoice.start();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (hasBeenTriggered == false)
        {
            hasBeenTriggered = true;
            if (collision.gameObject.tag == "Player")
            {
                print("TRANSFORM");
                GameObject.Find("willIAM").GetComponent<SpriteRenderer>().enabled = true ;
                theVOController.setVOIndexAndPlay(11);
                GameObject.Find("willIAM").GetComponent<Animator>().Play("William_Transform");
            }
        }

    }
}
