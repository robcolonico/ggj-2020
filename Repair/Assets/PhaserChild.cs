﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaserChild : MonoBehaviour
{
    public PhaserMaster phaserParent;
    public float timeToFullyDrill = 0;
    public float progress = 0;
    public bool finished = false;

    void StartBeingDrilled(int drillState)
    {
        timeToFullyDrill = (1 - progress) * timeToFullyDrill;
        StartCoroutine(DrillRoutine(timeToFullyDrill));
    }
    void StopBeingDrilled(int drillState)
    {
        StopAllCoroutines();
    }
    IEnumerator DrillRoutine(float timeToLerp)
    {
        float currentprogress = progress;
        float currentTime = 0;
        float fasterTime = 0;
        float rate = 1 / timeToLerp;
        float fasterRate = 2 / timeToLerp;
        while (currentTime < 1)
        {
            currentTime += Time.deltaTime * rate;
            fasterTime += Time.deltaTime * rate;

            progress = Mathf.Lerp(currentprogress, 1, currentTime);

            yield return null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (finished == false)
        {
            if (progress >= 1.0f)
            {
                finished = true;
                print("Phaser Increment Called");
                phaserParent.PhaserIncrement();
            }
        }
    }


}
