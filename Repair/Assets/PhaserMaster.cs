﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaserMaster : MonoBehaviour
{
    public int PhaserCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PhaserIncrement ()
    {
        ++PhaserCount;
        if (PhaserCount >= 3)
        {
            print("PhaserMaster has all it needs to do COOL STUFF");
        }
    }
}
